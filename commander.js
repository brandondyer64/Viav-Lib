const { client, discord } = require('./discord')
const plugin = require('./plugins/plugin')
const configs = require('./configs')
const { sendMessage } = require('./messager')
const { getLang } = require('./util/lang')

function applyCommands(commands) {
  client.on('message', message => {
    // Get guild config
    if (!message.guild) return
    const guildConfig = configs.guildDefault
    const lang = guildConfig.lang
    // Check if the message is for me
    if (
      message.mentions &&
      message.mentions.members.array()[0] === message.guild.me
    ) {
      sendMessage(message.channel, `Hi! \`${guildConfig.prefix}help\` for help`)
      return
    }
    // Check that message is a command
    if (!message.content.startsWith(guildConfig.prefix)) return
    // Get command params
    const params = message.content.substr(guildConfig.prefix.length).split(' ')
    const command = params[0].toLowerCase()
    params.shift()
    // Delete command
    /*
    setTimeout(() => {
      if (message.deletable) message.delete()
    }, 1000)*/

    if (guildConfig.deleteMessages === true) {
      message.delete()
    }
    // Check that command exists
    if (!(command in commands)) {
      sendMessage(
        message.channel,
        `The command **${guildConfig.prefix}${command}** doesn't exist! Try **${
          guildConfig.prefix
        }help**`,
        5,
        11023412
      )
      return
    }
    // Check if command requires admin
    if (commands[command].admin) {
      if (configSecret.admins.includes(message.author.id)) {
        // User has admin
        sendMessage(message.channel, 'Admin authorized.', 2, 2787114)
      } else {
        // User does not have admin
        sendMessage(
          message.channel,
          `You must be an admin to run this command.`,
          5,
          11023412
        )
        return
      }
    }
    // Check if command requires owner
    if (commands[command].owner) {
      if (message.member.id === message.member.guild.ownerID) {
        // User is owner
        sendMessage(message.channel, 'Owner authorized.', 2, 2787114)
      } else if (configSecret.admins.includes(message.author.id)) {
        // User has admin
        sendMessage(message.channel, 'Admin authorized.', 2, 2787114)
      } else {
        // User does not have admin
        sendMessage(
          message.channel,
          `You must be the server owner to run this command.`,
          5,
          11023412
        )
        return
      }
    }
    // Run command
    try {
      let callback = responseText => {
        // Echo command response
        if (responseText) {
          sendMessage(
            message.channel,
            responseText,
            commands[command].timeout || null
          )
        }
      }
      if (commands[command].plugin) {
        commands[command].run({
          params,
          message,
          callback,
          lang: getLang(message),
          guildConfig,
          configSecret: configs.secret,
          guild: message.guild
        })
      } else {
        commands[command].run(params, message, callback, getLang(message))
      }
    } catch (err) {
      if (configs.secret.test) {
        sendMessage(message.channel, err.stack, 10, 11023412)
      } else {
        sendMessage(message.channel, `**Whoops!** ${err.message}`, 10, 11023412)
      }
    }
  })
}

module.exports = {
  applyCommands
}
