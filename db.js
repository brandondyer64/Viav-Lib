const mysql = require('mysql')

function createConnection(configSecret) {
  db.con = mysql.createConnection({
    host: configSecret.database.host,
    user: configSecret.database.user,
    password: configSecret.database.pass,
    database: configSecret.database.database
  })
  db.con.connect()
}

const db = {
  createConnection,
  con: {}
}

module.exports = db
