module.exports = {
  start: require('./start'),
  login: require('./login'),
  client: require('./discord').client,
  configs: require('./configs'),
  plugins: require('./plugins/plugin').plugins,
  commands: require('./plugins/plugin').commands,
  createPlugin: require('./plugins/plugin').createPlugin,
  registerPlugins: require('./plugins/plugin').registerPlugins,
  setLocales: require('./util/lang').setLocales,
  helpCommand: require('./plugins/core/help')
}

require('./plugins/core')
