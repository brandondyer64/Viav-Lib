const discord = require('./discord')
const configs = require('./configs')
const db = require('./db')

var mysql = require('mysql')

module.exports = (config, configSecret, guildDefault) => {
  configs.config = config
  colorise(config.colors)
  configs.secret = configSecret
  configs.guildDefault = guildDefault
  discord.client.login(configs.secret.token)
  if (configSecret.database.enable) db.createConnection(configSecret)
}

function colorise(colors) {
  for (let i in colors) {
    if (typeof colors[i] === 'string') {
      colors[i] = parseInt('0x' + colors[i])
    }
  }
}
