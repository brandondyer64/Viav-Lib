const { getConfig } = require('./util/guildFunctions')
const configs = require('./configs')

/**
 * Send a message from the bot.
 *
 * @param  {Number} channel         The channelID to send the message in.
 * @param  {String} message         The message itself.
 * @param  {Number} [timeout=null]  The timeout for the message to self destruct.
 * @param  {Number} [color=7506394] The embed border color.
 */
exports.sendMessage = (channel, message, timeout = null, color = null) => {
  console.log('send message')
  if (color === null) color = configs.colors.message
  channel
    // Send as embed
    // TODO: This code seems a little overly complicated, consider rewriting to reduce complexity?
    .send(
      typeof message === 'string' &&
      !message.includes('\n') &&
      !message.includes('[') &&
      color === 7506394
        ? '\u200B' + message
        : {
            embed:
              typeof message !== 'string'
                ? // Response is a rich value
                  '\u200B' + message
                : // Response is a plaintext string
                  {
                    color: color,
                    description: '\u200B' + message
                  }
          }
    )
    // Delete response after a timeout
    .then(responseMessage => {
      const guildConfig = getConfig(channel.guild.id)
      // Set timeout only if it's greater than 0 seconds
      if (timeout == null) {
        if (typeof message === 'string') {
          timeout = message.length / 8 + 2
        } else {
          timeout = 30
        }
      }

      // Check that timeout isn't zero
      if (timeout && guildConfig.deleteMessages === true) {
        setTimeout(() => {
          responseMessage.delete()
        }, timeout * 1000)
      }
    })
    .catch(console.error)
}

/**
 * Send a DM to a specified user.
 *
 * @param  {[type]} user    The user to send a message to.
 * @param  {String} message The message itself.
 * @return {Promise}
 */
exports.sendDM = (user, message) => {
  return new Promise((resolve, reject) => {
    let send = channel => {
      channel.send(message)
    }
    if (user.dmChannel) {
      send(user.dmChannel)
      resolve(user.dmChannel)
    } else {
      user
        .createDM()
        .then(send)
        .then(() => {
          resolve(user.dmChannel)
        })
    }
  })
}
