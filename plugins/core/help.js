const core = require('./index')
const { commands } = require('../plugin')

const format = {
  pre({ lang, configSecret }) {
    return (
      // Hi! I'm Bot!
      `**${lang.hello}**\n${lang.Im} ` +
      `**${configSecret.name}**!` +
      // Command List
      `\n\n**${lang.command_list}:**\n`
    )
  },
  command({ lang, prefix, command, plugin }) {
    return `\`${prefix}\`**${command}** _- ${plugin}_\n`
  },
  post({ lang, configSecret }) {
    return ''
  }
}

core.command('help', {
  alias: ['h', 'info'],
  description: 'Gives a command list or description of a command',
  run({ params, message, callback, lang, guildConfig, configSecret }) {
    if (params.join(' ') === '') {
      let output = format.pre({ lang, configSecret })
      // List commands
      for (let i in commands) {
        const command = commands[i]
        // Check conditions to print command
        if (
          // Plugin is disabled
          (!command.plugin || guildConfig.plugins[command.plugin] !== false) &&
          // Command is hidden
          !command.hidden &&
          (!(command.owner || command.admin) || // Command doesn't require owner or admin
            // Author is admin
            configSecret.admins.includes(message.author.id) ||
            // Author is owner
            (!command.admin &&
              message.member.guild.owner.id === message.member.id))
        ) {
          let plugin = command.plugin
          // Print command
          output += format.command({
            prefix: guildConfig.prefix,
            command: i,
            lang,
            plugin
          })
        }
      }
      output += format.post({ lang, configSecret })
      callback(output)
    } else {
      // Get command description
      let description
      if (lang[`command_${params[0]}`]) {
        description = lang[`command_${params[0]}`]
      } else {
        description = commands[params[0]].description
      }
      // Echo description
      callback(`**${params[0]}:**\n${description}`)
    }
  }
})

module.exports = {
  format
}
