const { createPlugin } = require('../plugin')

const plugin = createPlugin('Core')

plugin.command('ping', {
  run({ callback }) {
    callback('pong')
  }
})

module.exports = plugin

require('./help')
