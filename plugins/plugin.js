const { applyCommands } = require('../commander')

const plugins = {}
const commands = {}

function createPlugin(pluginName) {
  const plugin = {
    name: pluginName,
    commands: {},
    command: (name, command) => {
      plugin.commands[name] = command
      if (command.alias) {
        for (var i in command.alias) {
          let alias = command.alias[i]
          let aliasCommand = Object.assign({}, command)
          plugin.commands[alias] = aliasCommand
          delete aliasCommand.alias
          aliasCommand.hidden = true
        }
        delete command.alias
      }
      return plugin
    },
    clientOn: (name, event) => {
      client.on(name, (...params) => {
        const guildConfig = getConfig(event.guild(params).id)
        if (guildConfig.plugins[pluginName]) {
          event.run(params)
        }
      })
      return plugin
    }
  }
  plugins[pluginName] = plugin
  return plugin
}

function registerPlugins() {
  console.log('\n')
  console.log('If you like viav.js, consider donating.')
  console.log('https://patreon.com/viav')
  console.log('\n')
  console.log('Loading plugins:')
  // Get all plugins
  for (let pluginI in plugins) {
    let plugin = plugins[pluginI]
    process.stdout.write(pluginI + ' ')
    // Get plugin's commands
    for (commandI in plugin.commands) {
      process.stdout.write('.')
      let command = plugin.commands[commandI]
      // Add plugin data to command
      command.plugin = pluginI
      commands[commandI] = command
    }
    console.log('')
  }
  applyCommands(commands)
  console.log('Done.')
}

module.exports = {
  plugins,
  commands,
  createPlugin,
  registerPlugins
}
