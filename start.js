const discord = require('./discord')
const configs = require('./configs')

module.exports = (botFile, configSecret, guildDefault, options = {}) => {
  console.log(`Starting Viav.js Bot: ${configSecret.name}`)
  configs.secret = configSecret
  configs.guildDefault = guildDefault
  if (!options.test) {
    const manager = new discord.discord.ShardingManager(botFile, {
      token: configSecret.token
    })
    manager.on('launch', shard =>
      console.log(`Successfully launched shard ${shard.id}`)
    )
    manager.spawn()
  } else {
    configs.secret.test = true
    require(botFile)
  }
}
