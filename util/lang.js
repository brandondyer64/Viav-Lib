const { mergeDeep } = require('./objectMerger')
const { getConfig } = require('./guildFunctions')

// Compile languages
const langson = {}
exports.langson = langson

exports.setLocales = (...locales) => {
  for (i in locales) {
    langson[locales[i].locale] = {
      ...locales[0],
      ...locales[i]
    }
  }
}

// Return a string based on locale and key
exports.langl = (locale, key) => {
  return langson[locale][key]
}

// Return a string based on a key and locale derived from guild config
exports.lang = (message, key) => {
  return exports.langl(getConfig(message.guild.id)['lang'], key)
}

// Return a language based on locale derived from guild config
exports.getLang = message => {
  return langson[getConfig(message.guild.id)['lang']]
}
